<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    //Método cargar Lista de Usuarios
    public function index() {
        $users = User::all();
        return \View::make('users/list',compact('users'));
    }

    //Método para cargar vista Update en la carpeta users
    public function edit($id) {
        $user = User::find($id);
          
        return \View::make('users/update', compact('user'));
    }
    
    //Método para actualizar el usuario selecionado 
    public function update($id,Request $request)
    {
          $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'lastname'=>'required|string|max:255',
            'document'=>'required|string|max:255'
            
        ]);
          
        $user = User::find($id);
        $user->name=$request->name;
        $user->lastname=$request->lastname;
        $user->email=$request->email;
        $user->document=$request->document;
        $user->save();

        return redirect('admin/user');

    }


    public function destroy($id) 
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie as Movie;

class MovieController extends Controller
{
     //Método cargar Lista de Peliculas
	public function index()
	{
		$movies=Movie::all();
		return \View::make('movies/list',compact('movies'));
	}
    //Método para cargar vista New en la carpeta movies
	public function create()
	{
		return \View::make('movies/new');
	}

    //Método Guardar Movie
    public function store(Request $request)
    {
    	 $request->validate([
            'name' =>'required|min:3|regex:/^[\pL\s\-]+$/u',
            'description' =>'required|min:6|regex:/^[\pL\s\-]+$/u'
        ]);
        $movie = new Movie;
        $movie->name = $request->name;
        $movie->description = $request->description;
        $movie->category=$request->category;
        $movie->user_id = \Auth::user()->id;
    	$movie->save();
    	return redirect('movie');
    }
    
    //Método para cargar vista Update en la carpeta movies
    public function edit($id)
    {
    	$movie = Movie::find($id);
    	return \View::make('movies/update',compact('movie'));
    }
    
     //Método Editar Movie
    public function update($id,Request $request)
    {
    	$movie = Movie::find($id);
    	$movie->name=$request->name;
    	$movie->description=$request->description;
        $movie->category=$request->category;
    	$movie->save();
    	return redirect('movie');

    }
    
    //Método para eliminar movie
    public function destroy($id) 
    {
        $movie = Movie::find($id);
        $movie->delete();
        return redirect()->back();
    }
}

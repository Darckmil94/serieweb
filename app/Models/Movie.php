<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
     protected $table = "movies";
    protected $fillable = ['name', 'description','category','user_id'];
    protected $guarded = ['id'];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}


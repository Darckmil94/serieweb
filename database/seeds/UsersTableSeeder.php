<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->name='Juan';
        $user->lastname='pagon';
        $user->email='judapagon1996@hotmail.es';
        $user->password=Hash::make('Dpasaje10');
        $user->role='Admin';
        $user->document='1014285469';
        $user->save();
    }
}

@extends('layouts.app')
@section('content')
   <section class="container">
   	  <div class="row">
   	  	 <article class="col-md-10 col-md-offser-1">
   	  	 	{!! Form::open(['route'=>'movie.store','method'=>'post','novalidate']) !!}
   	  	 	<div class="form-group">
   	  	 		<label>Nombre</label>
   	  	 		<input type="text" name="name" class="form-control" required>
   	  	 	</div>
   	  	 	<div class="form-group">
   	  	 		<label>Descripcion</label>
   	  	 		<input type="text" name="description" class="form-control" required>
   	  	 	</div>
             <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Categoria</label>
                            <div class="col-md-6">
                                <select name="category" class="col-md-12 form-control">
                                    <option value="">Seleccione</option>
                                    <option value="Serie">Serie</option>
                                    <option value="Pelicula">Pelicula</option>                                    
                                </select>
                            </div>
                        </div>
   	  	 	<div class="form-group">
   	  	 		<button type="submit" class="btn btn-success">Enviar</button>
   	  	 	</div>
   	  	 	{!! Form::close() !!}
   	  	 </article>
   	  </div>
   </section>
@endsection   
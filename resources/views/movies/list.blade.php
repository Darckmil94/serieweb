@extends('layouts.app')
@section('content')
  <section class="container">
  	 <div class="row">
  	 	 <article class="col-md-12">
        <div class="table-responsive">
  	 	 	<table class="table table-condensed table-striped table-bordered" >
  	 	 		<thead class="thead-dark">
  	 	 			<tr>
  	 	 				<th> Nombre </th>
  	 	 				<th> Descripcion </th>
              <th> Categoria </th>
  	 	 				<th> Accion  </th>
  	 	 			</tr>
  	 	 		</thead>
  	 	 		<tbody>
  	 	 			@foreach($movies as $movie)
  	 	 			  <tr>
  	 	 			     <td>{{$movie->name}}</td>
  	 	 			     <td>{{$movie->description}}</td>
                 <td>{{$movie->category}}</td>
  	 	 			     <td>
  	 	 			     	<a class="btn btn-primary btn-xs" href="{{ route('movie.edit',['id'=>$movie->id]) }}">Editar</a>
    	 			     	<a class="btn btn-danger btn-xs " href="{{ route('movie/destroy',['id' => $movie->id]) }}">Eliminar</a>
  	 	 			     </td>	
  	 	 			  </tr>
  	 	 			  @endforeach
  	 	 		</tbody>
  	 	 	</table>
        </div> 	 	
  	 	 </article>
         @if(\Auth::user()->role == 'Admin')
       <article class="col-md-12">
        {!! Form::open(['route'=>'movie.store','method'=>'post','novalidate','class'=>'form-inline']) !!} 
            
                <div class="ml-2">
                  <a href="{{ route('movie.create') }}" class="btn btn-primary">Create</a>  
                </div>          
        {!! Form::close() !!}
       </article>
       @endif
  	 </div>
  </section>
  @endsection
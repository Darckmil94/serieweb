@extends('layouts.app')
@section('content')
	<section class="container">
		<div class="row">			
			<article class="col-md-12">
				<div class="table-responsive">
				<table class="table table-condensed table-striped table-bordered">
					<thead class="thead-dark">
						<tr>
							<th>Apellido</th>
							<th>Nombre</th>
							<th>Email</th>
							<th>Documento</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>						
						@foreach($users as $user)
							<tr>
								<td>{{ $user->lastname }}</td>
								<td>{{ $user->name }}</td>
								<td>{{ $user->email }}</td>
								<td>{{ $user->document }}</td>
								<td>
									<a class="btn btn-primary btn-xs" href="{{ route('user.edit',['id' => $user->id]) }}">Editar</a>
									<a class="btn btn-danger btn-xs" href="{{ route('user/destroy',['id' => $user->id]) }}">Eliminar</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
				</div>
			</article>
		</div>
	</section>
@endsection
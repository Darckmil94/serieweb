<?php

	Route::resource('movie','MovieController');	
	Route::get('movie/destroy/{id}', ['as' => 'movie/destroy', 'uses' => 'MovieController@destroy']);
	Route::post('movie/show', ['as' => 'movie/show', 'uses' => 'MovieController@show']);
	Route::post('movie/store', ['as' => 'movie/store', 'uses' => 'MovieController@store']);

	Route::resource('user','UserController');
	Route::get('user/destroy/{id}', ['as' => 'user/destroy', 'uses' => 'UserController@destroy']);
	Route::post('user/show', ['as' => 'user/show', 'uses' => 'UserController@show']);